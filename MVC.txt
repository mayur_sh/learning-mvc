MVC
model view controller
logic, design and data separately
php code - controllers
html - views
data which we get from db - models
special function __autoload that autoloads are classes first
routes - how we navigate around our app, when we create new page we create new route
controller is for every single page, where the logic goes
modify our htaccess file, any request is made we send them to index.php page which pass the url parameter which tells that on which page they want to send

ROUTES-
	First we autoload all the classes on index.php then make a php file routes.php in which we use the route class and set function, the route class is made in classes folder, set all the different routes in routes.php and in route class make a property named valid routes and make it an array then make set function and pass the $route and $function in it and then inside that array pass all the route so that we can easily check that the route is valid or not

CONTROLLER-
	we make a directory named controller in which we will have separate file for all pages
	there will be a base file called controller in which we will add basic functionality or feature which all the controller should have and then in all the files extend that base class controller

VIEWS-
	in the views folder html with same name
	pass from routes to controller and then controller to views

MODELS-
	interacting with DB

