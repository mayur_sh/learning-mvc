<?php
require_once('Routes.php');
//echo $_GET['url'];
//for autoloading our classes so that we don't need to include routes class 

function __autoload($class_name){
	if(file_exists('./classes/'.$class_name.'.php')){
		require_once './classes/'.$class_name.'.php';
	}else if(file_exists('./Controllers/'.$class_name.'.php')){
		require_once './Controllers/'.$class_name.'.php';
	}
}

?>